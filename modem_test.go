package tasio

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

var (
    testDevices = []string {
        "/dev/ttyUSB0",
        "/dev/ttyUSB3",
    }
)

func TestConnect(t *testing.T) {
    for _, name := range testDevices {
        m := Modem{}
        m.Name = name
        err := m.Connect()

        assert.NoError(t, err)
        assert.NotNil(t, m.Conn)
    }
}

func TestRun(t *testing.T) {
    res := "OK"
    cmd := "AT"

    m := Modem{}
    m.Name = testDevices[0]
    err := m.Connect()

    assert.NoError(t, err)
    assert.NotNil(t, m.Conn)
    assert.Equal(t, res, m.Run(cmd, true))
}