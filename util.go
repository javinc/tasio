package tasio

import (
    l "log"
    "fmt"
    "bytes"
    "strings"
    "net/http"
    "github.com/kardianos/osext"
)

var (
	Verbose = false
)

func output(v... interface{}) {
	fmt.Println(v...)
}

func log(v... interface{}) {
	if Verbose {
		l.Println(v...)
	}
}

func exit(v... interface{}) {
	if Verbose {
		l.Fatal(v...)
	}
}

func getExecutableDir() string {
    p, err := osext.Executable()
    if err != nil {
        l.Fatal(err)
    }

    // get exectable file
    sep := "/";
    a := strings.Split(p, sep)

    return strings.TrimSuffix(p, sep + a[len(a) -1]) + sep
}

func sendPost(url, data string) {
    // send post json
    req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(data)))
    req.Header.Set("Content-Type", "application/json")

    log("requesting ...")
    client := &http.Client{}
    r, err := client.Do(req)
    if err != nil {
        output(err)
    }

    defer r.Body.Close()

    log("response status is", r.Status)
}