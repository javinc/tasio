package tasio

import (
	"errors"
	"os"
	"strings"
	"time"

	"github.com/haxpax/goserial"
)

type Modem struct {
	Name string
	Conn serial.ReadWriteFlushCloser
}

var (
	baud    = 115200
	maxLoop = 10
)

func (x *Modem) Connect() error {
	if x.Name == "" {
		return errors.New("Modem Error: Name not defined")
	}

	if _, err := os.Stat(x.Name); err != nil {
		return errors.New("Modem Error: Name not exists")
	}

	log("Modem Connect on", x.Name)

	c := &serial.Config{
		Name:        x.Name,
		Baud:        baud,
		ReadTimeout: time.Second,
	}

	s, err := serial.OpenPort(c)
	log(err)
	if err == nil {
		// check if responding
		err = check(s)
		if err != nil {
			log("Modem Connection Bad")

			return err
		}

		log("Modem Connection Connect Good", s)
		x.Conn = s
	}

	return nil
}

func (x *Modem) Run(cmd string, wait bool) string {
	var status string
	if cmd != "" {
		log("Modem Command", cmd)

		x.Conn.Flush()

		_, err := x.Conn.Write([]byte(cmd + "\r"))
		if err != nil {
			exit(err)
		}
	}

	buf := make([]byte, 32)
	loop := 0
	for loop < maxLoop {
		n, _ := x.Conn.Read(buf)
		if n > 0 {
			status = status + string(buf[:n])
			log("Modem Received", n, "bytes: ", status)
			if !wait || strings.HasSuffix(status, ">\r\n") || strings.HasSuffix(status, "OK\r\n") || strings.HasSuffix(status, "ERROR\r\n") {
				break
			}
		}

		loop++
	}

	return strings.TrimSpace(status)
}

func (x *Modem) Number() string {
	log("Modem Number")

	return strings.TrimSpace(x.Run("AT+CNUM", true))
}

func (x *Modem) Imei() string {
	log("Modem Imei")

	raw := strings.Split(x.Run("AT+CGSN", true), "\n")
	return strings.TrimSpace(raw[0])
}

func check(conn serial.ReadWriteFlushCloser) error {
	log("Modem Check")

	conn.Flush()

	_, err := conn.Write([]byte("AT+CMGF=?\r"))
	if err != nil {
		return err
	}

	n, _ := conn.Read(make([]byte, 32))
	if n == 0 {
		return errors.New("Device not responding")
	}

	return nil
}
