package tasio

import (
	"errors"
	"strconv"
	"strings"
)

const (
	COMMAND_SET = "AT+CMGF=?"
	CR          = "\015"
	SUB         = "\032"

	MODE_PDU  = "AT+CMGF=0"
	MODE_TEXT = "AT+CMGF=1"

	SEND_PREFIX = "AT+CMGS="

	RECEIVED_ALL  = "AT+CMGL=4"
	RECEIVED_READ = "AT+CMGL=1"
	RECEIVED_NEW  = "AT+CMGL=0"

	DELETE_ALL   = "AT+CMGD=0,4"
	DELETE_READ  = "AT+CMGD=0,1"
	DELETE_INDEX = "AT+CMGD="

	STORAGE_SIM = "AT+CPMS=\"SM\",\"SM\",\"SM\""
)

func (x *Modem) Verify() string {
	return x.Run(COMMAND_SET, false)
}

func (x *Modem) Send(address, text string) (string, error) {
	log("SMS Send")

	var s string

	// check address length
	// lengthAddress := len(address)
	// if lengthAddress != 13 {
	// 	return s, errors.New("SMS ERROR: invalid length address, must be 13")
	// }

	lengthText := len(text)
	if lengthText > 160 {
		return s, errors.New("SMS ERROR: invalid length text, must be 160")
	}

	// clear
	x.Run("AT"+CR, false)
	log("Sending", address, text)

	// sending sms in pdu mode
	x.Run(MODE_PDU+CR, false)

	// encode pdu
	length, pdu := Encode(address, text)

	// conver int length to string
	r := x.Run(SEND_PREFIX+strconv.Itoa(length)+CR, false)

	if strings.Index(r, "ERROR") > -1 {
		return r, errors.New("SMS ERROR: sending failed")
	}

	// send the pdu
	r = x.Run(pdu+SUB, true)

	// clear
	x.Run("AT"+CR, false)

	if strings.Index(r, "ERROR") > -1 {
		return r, errors.New("SMS ERROR: sending failed")
	}

	return r, nil
}

func (x *Modem) Read() (map[int]Message, error) {
	log("SMS Read")

	// set storage on read, send, new message to sim
	x.Run(STORAGE_SIM, true)

	// get all received messages in PDU mode
	x.Run(MODE_PDU+CR, true)
	raw := x.Run(RECEIVED_ALL+CR, true)

	// parse data
	s := strings.Split(raw, "\n")

	// get all messages
	// var msgs []Message
	msgs := make(map[int]Message)
	index := -1
	for _, row := range s {
		// decode messages

		// getting index
		i := getIndex(row)
		if i != -1 {
			index = i
		}

		// if prefixed by 07 means its PDU
		if strings.Index(row, "07") == 0 && index != -1 {
			msg := Decode(row)

			// normalize address prefixes
			// check address if start with 09
			if strings.Index(msg.Address, "09") == 0 {
				msg.Address = strings.TrimPrefix(msg.Address, "0")
				msg.Address = "+63" + msg.Address
			}

			msgs[index] = msg
		}
	}

	// check if no msgs
	if len(msgs) == 0 {
		return nil, nil
	}

	return msgs, nil
}

func (x *Modem) Delete(i int) error {
	log("SMS Delete")

	if !strings.HasSuffix(x.Run(DELETE_INDEX+strconv.Itoa(i)+CR, true), "OK") {
		return errors.New("SMS ERROR: deletion error")
	}

	return nil
}

func (x *Modem) Extras() {
	log("SMS Extras")

	output(x.Number())
	// output(x.Imei())
}

func getIndex(s string) int {
	i := -1

	// check prefix
	prefix := "+CMGL: "
	delimiter := ","
	if strings.Index(s, prefix) == 0 {
		// get first element
		rawDetails := strings.Split(s, delimiter)[0]
		index := strings.Trim(rawDetails, prefix)

		// cast index to int
		i, err := strconv.ParseInt(index, 0, 0)
		if err != nil {
			i = -1
		}

		return int(i)
	}

	return i
}
