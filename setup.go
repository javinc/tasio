package tasio

import (
	"encoding/json"
	"io/ioutil"
	"os/exec"
	"sort"
	"strconv"
	"strings"
)

func Install() {
	output("installing driver...")

	// exucute driver installation
	installDriver()

	// own device ports
	output("owning ports...")
	out, _ := exec.Command("chown", "$USER:$USER", "/dev/ttyUSB*").Output()
	log(string(out))

	// list ports
	modems := Devices()
	if len(modems) == 0 {
		output("modem not found! \n")
		return
	}

	// get config
	GetConfig(true)

	// clear ports first
	clearPorts()

	// set ports
	setPorts(modems)
}

func installDriver() {
	// make interface
	out, _ := exec.Command("usb_modeswitch", "-v", "0x12d1", "-p", "0x1446",
		"-M", "55534243000000000000000000000011060000000000000000000000000000",
		"-V", "0x12d1", "-P", "0x1436", "-s", "3", "-m", "0x01").Output()
	log(string(out))
}

func clearPorts() {
	output("clearing config...")
	for i := range Config.Devices {
		// clear ports
		Config.Devices[i].Port = ""
		Config.Devices[i].Notif = ""
	}

	b, err := json.MarshalIndent(Config, "", "  ")
	if err != nil {
		output(err)

		return
	}

	// write a config
	err = ioutil.WriteFile(getExecutableDir()+CONFIG_FILE, b, 0777)
	if err != nil {
		output(err)

		return
	}

	output("config is cleared! \n")
}

func setPorts(m map[string]Modem) {
	// store ports by imei
	output("storing ports by imei")
	imeis := make(map[string]string)
	for _, modem := range m {
		// record imei
		portNum := strings.Replace(modem.Name, DEVICE_PATH+PORT_PREFIX, "", 1)
		imeis[portNum] = modem.Imei()
		output("recording ", imeis[portNum], " for ", modem.Name, "...")
	}

	i := 0
	keys := make([]int, len(imeis))
	for port := range imeis {
		n, _ := strconv.Atoi(port)
		// keys = append(keys, n)
		keys[i] = n
		i++
	}

	output("sorting imei by port name...")
	sort.Ints(keys)

	countCheck := 0
	for _, k := range keys {
		p := strconv.Itoa(k)

		name := DEVICE_PATH + PORT_PREFIX + p
		for i, c := range Config.Devices {
			// search for imei
			output("searching", c.Imei, "...")
			if c.Imei != imeis[p] {
				continue
			}

			output("found", name, "!")
			// assign send port first
			if len(c.Port) == 0 {
				Config.Devices[i].Port = name
			} else {
				Config.Devices[i].Notif = name
			}
			countCheck++
			break
		}
	}

	// check error
	// one device is = one imei
	// and it require 2 ports per device
	output("\nchecking ports vs devices...")
	output(countCheck, "/", len(Config.Devices)*2, "ports found!")
	if len(Config.Devices)*2 != countCheck {
		output("config is bad!")
		output("installing again... \n")
		Install()

		return
	}

	b, err := json.MarshalIndent(Config, "", "  ")
	if err != nil {
		output(err)

		return
	}

	output(string(b))

	// write a config
	err = ioutil.WriteFile(getExecutableDir()+CONFIG_FILE, b, 0777)
	if err != nil {
		exit(err)
	}

	output("config is good!")
}
