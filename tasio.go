package tasio

import (
	"encoding/json"
	"io/ioutil"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"
)

// SMS message with high level representation
// complies with 3GPP TS 23.040
type Message struct {
	SCA       string
	Address   string
	Text      string
	TimeStamp time.Time
}

// messages used on database
type DatabaseMessage struct {
	ID        int
	Status    int
	Device    string
	SCA       string `sql:"size:13"`
	Address   string `sql:"size:13"`
	Text      string `sql:"size:160"`
	TimeStamp time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// table of received messages
// status 0 is new
// status 1 is read
type Inbox DatabaseMessage

// table of queued messages
// status 0 is pending
// status 1 is done
// status 2 is priority
// status 3 is error
// status 4 is sending
type Outbox DatabaseMessage

// config json
type Configuration struct {
	Ratatxt struct {
		Host  string
		Token string
	}
	Devices []struct {
		Id     string
		Url    string
		Port   string
		Notif  string
		Number string
		Imei   string
	}
}

const (
	VERSION = "v0.7.1"

	CONFIG_FILE = "config.json"

	// specific for *Nix os
	DEVICE_PATH = "/dev/"
	PORT_PREFIX = "ttyUSB"

	LINE = "--------------------------------"

	CHUP = "AT+CHUP"
)

var (
	Config  Configuration
	wg      sync.WaitGroup
	Origins = map[string]string{}
)

func Init() (Db, map[string]Modem) {
	// initialize db
	db := Db{}
	err := db.Init()
	if err != nil {
		exit(err)
	}

	// create connections
	modems := make(map[string]Modem)
	for _, device := range Config.Devices {
		// initialize modem
		modem := Modem{Name: device.Port}
		err = modem.Connect()
		if err != nil {
			exit(err)
		}

		modems[device.Id] = modem
		// initialize notifs port
		go notif(device.Id, device.Url, device.Notif, modem)

		Origins[device.Id] = device.Number
	}

	return db, modems
}

// loop starter the modem watcher
func Start(interval int) {
	// get configuration from config.json
	GetConfig(true)

	output("start", interval, "interval")

	loop := 1
	db, modems := Init()
	for deviceId, modem := range modems {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				log("loop ", loop)
				loop++

				if Config.Ratatxt.Host != "" {
					// sending from ratatxt outbox
					for _, msg := range RGetOutbox(Origins[deviceId]) {
						output("ratatxt sending", msg.Id, msg.Address, msg.Text)
						_, err := modem.Send("+"+msg.Address, msg.Text)
						if err == nil {
							// set status to done
							RUpdateOutbox(msg.Id, "done")
							output("ratatxt sent", msg.Id, msg.Address, msg.Text)
						} else {
							// set status to error
							RUpdateOutbox(msg.Id, "error")
							output("ratatxt failed", err)
						}
					}
				}

				// send msgs
				log("sending ", loop)
				outbox, _ := db.GetPending()
				db.GetPending()
				if outbox.ID != 0 {
					// check if device exists
					if _, ok := modems[outbox.Device]; !ok {
						db.UpdateOutboxByStatus(int(outbox.ID), 3)

						output("deviceId not found", outbox.Device)
					} else if outbox.Device == deviceId {
						// send msgs
						sendMsgs(deviceId, outbox, modem, db)
					}
				}

				// get msgs
				readMsgs(deviceId, modem, db)
				log("reading ", loop)

				time.Sleep(time.Duration(interval) * time.Millisecond)
			}
		}()
	}

	wg.Wait()
}

func sendMsgs(deviceId string, outbox Outbox, modem Modem, db Db) {
	// mark msg as sending
	db.UpdateOutboxByStatus(int(outbox.ID), 4)

	// get msg queue with status 0
	_, err := modem.Send(string(outbox.Address), string(outbox.Text))
	if err == nil {
		// set status to done
		db.UpdateOutboxByStatus(int(outbox.ID), 1)

		output("sent", outbox.ID, outbox.Address, outbox.Text)
	} else {
		// set status to error
		db.UpdateOutboxByStatus(int(outbox.ID), 3)

		output(err)
	}
}

func readMsgs(deviceId string, modem Modem, db Db) {
	msgs, err := modem.Read()
	if err == nil {
		for i, msg := range msgs {
			if Config.Ratatxt.Host != "" {
				// save to ratatxt inbox
				RPostInbox(Origins[deviceId], msg.Address, msg.Text)
			}

			// save to db
			err = db.CreateInbox(msg, deviceId)
			if err != nil {
				output(err)
			}

			// delete by index
			err = modem.Delete(i)
			if err != nil {
				output(err)
			}
		}

		// dont display if no msgs
		if len(msgs) != 0 {
			output("received", msgs)
		}
	}
}

// send to queue
// this will create on outbox table
// and status by 0 means pending by default
func Send(deviceId, address, text string) {
	output("send", deviceId, address, text)

	db, _ := Init()
	err := db.CreateOutbox(Message{
		Address: address,
		Text:    text,
	}, deviceId)

	if err != nil {
		output(err)

		return
	}

	output("queued")
}

// read messages
// it will read from inbox table
// and status by 1 to marked as read
func Read(mode string) {
	output("read", mode)

	db, _ := Init()

	var msgs []Inbox
	var err error
	if mode == "new" {
		msgs, err = db.GetInboxByStatus(0)
		if err != nil {
			output(err)

			return
		}

		// update to read
		for _, msg := range msgs {
			db.UpdateInboxByStatus(int(msg.ID), 1)
		}
	} else {
		// else all
		msgs, err = db.GetInbox()
		if err != nil {
			output(err)

			return
		}
	}

	// json encode
	j, _ := json.Marshal(msgs)

	output(string(j))
}

// get available devices
func Devices() map[string]Modem {
	output("list available devices")

	// find device path using with prefix ttyUSB
	out, err := exec.Command("ls", DEVICE_PATH).Output()
	if len(out) == 0 || err != nil {
		output("error", err)

		exit()
	}

	modems := make(map[string]Modem)

	dev := strings.Split(string(out), "\n")
	for _, row := range dev {
		// decode messages
		// if prefixed by 07 means its PDU
		if strings.Index(row, PORT_PREFIX) == 0 {
			// check if
			modem := Modem{}
			modem.Name = DEVICE_PATH + row
			err := modem.Connect()
			if err != nil || modem.Conn == nil {
				continue
			}

			output(LINE)
			output(modem.Name)
			output(LINE)
			modem.Extras()

			// register good ports
			modems[row] = modem
		}
	}

	return modems
}

// get statistics
func Stat() {
	db, _ := Init()

	newInbox, _ := db.GetInboxByStatus(0)
	readInbox, _ := db.GetInboxByStatus(1)
	pendingOutbox, _ := db.GetOutboxByStatus(0)
	doneOutbox, _ := db.GetOutboxByStatus(1)
	errorOutbox, _ := db.GetOutboxByStatus(3)

	d := `{`
	d += `"inbox": {"new": ` + strconv.Itoa(len(newInbox))
	d += `, "read": ` + strconv.Itoa(len(readInbox))
	d += `}, "outbox": {"pending": ` + strconv.Itoa(len(pendingOutbox))
	d += `, "done": ` + strconv.Itoa(len(doneOutbox))
	d += `, "error": ` + strconv.Itoa(len(errorOutbox))
	d += `}}`

	output(d)
}

// get current config
func Version() {
	output(LINE)
	output("Tasio " + VERSION)
	output("Copyright (c) 2016-2017 javincX")
	output(LINE)
}

// get current config
func GetConfig(indent bool) {
	log("parsing config file")

	json.Unmarshal(jsonConfig(), &Config)

	s, _ := json.Marshal(Config)
	if indent {
		s, _ = json.MarshalIndent(Config, "", "  ")
	}

	output(string(s))

	if len(Config.Devices) == 0 {
		output("error parsing json file")
	}
}

// get config
func jsonConfig() []byte {
	out, err := exec.Command("pwd").Output()
	if len(out) == 0 || err != nil {
		exit(err)
	}

	file, err := ioutil.ReadFile(getExecutableDir() + CONFIG_FILE)
	if err != nil {
		exit(err)
	}

	return []byte(file)
}

// process notifications
func notif(deviceId, url, port string, modem Modem) {
	modem.Run("AT+CLIP=1", true)

	n := Modem{Name: port}
	err := n.Connect()
	if err != nil {
		exit(err)
	}

	signalQuality := "0"
	callerAddress := ""

	// read loop
	for {
		s := n.Run("", true)

		// catch blanks
		if s == "" {
			callerAddress = ""
		}

		// check calls
		go func() {
			clip := "+CLIP: "
			if strings.Index(s, clip) > -1 {
				// hung up the call
				modem.Run(CHUP, true)

				// parse string, get caller number
				for _, r := range strings.Split(s, "\n") {
					// check prefix
					if strings.HasPrefix(r, clip) {
						caller := strings.Split(strings.TrimPrefix(r, clip), ",")

						// catching continues +CLIP readings
						// event its been hunged up
						// that causes duplicate event
						if callerAddress == caller[0] {
							output(deviceId + " rejected call from " + callerAddress + ". call event not triggered")
							break
						}

						callerAddress = caller[0]

						// send event to callback
						go sendPost(url, `{"event": "call", "device": "`+
							deviceId+`", "data": {"address": `+callerAddress+`}}`)

						output(deviceId + " rejected call from " + callerAddress)
						break
					}
				}
			}
		}()

		// check signal quality
		go func() {
			csq := "+CSQ: "
			if strings.Index(s, csq) > -1 {
				// parse string, get signal
				for _, r := range strings.Split(s, "\n") {
					// check prefix
					if strings.HasPrefix(r, csq) {
						signal := strings.Split(strings.TrimPrefix(r, csq), ",")

						// skip when quality is same
						if signalQuality == signal[0] {
							break
						}

						signalQuality = signal[0]

						// send event to callback
						go sendPost(url, `{"event": "signal", "device": "`+
							deviceId+`", "data": {"quality": `+signalQuality+`}}`)

						output(deviceId + " signal quality " + signalQuality)
						break
					}
				}
			}
		}()
	}
}
