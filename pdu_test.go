package tasio

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

var (
    testSize = 23
    testPdu = "0001000C9136393507682600000BC8329BFD0651C3F3F41B"

    testAddr = "+639353708662"
    testText = "Hello Tasio"
)

func TestDecode(t *testing.T) {
    s, p := Encode(testAddr, testText)

    assert.Equal(t, testSize, s)
    assert.Equal(t, testPdu, p)
}

func TestEncode(t *testing.T) {
    m := Decode(testPdu)

    assert.Equal(t, testAddr, m.Address)
    assert.Equal(t, testText, m.Text)
}