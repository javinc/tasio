package main

import (
	"flag"

	"bitbucket.org/javinc/tasio"
)

var (
	start   = flag.Bool("start", false, "start the loop")
	send    = flag.String("send", "", "send message")
	read    = flag.Bool("read", false, "get received messages")
	devices = flag.Bool("devices", false, "list available devices")
	install = flag.Bool("install", false, "install modem drivers")
	config  = flag.Bool("config", false, "get current config")
	version = flag.Bool("version", false, "tasio version")
	stat    = flag.Bool("stat", false, "message statistics")

	verbose = flag.Bool("verbose", false, "show logs")

	interval = flag.Int("interval", 0, "interval in milli sec")

	new = flag.Bool("new", false, "read new messages")

	address = flag.String("address", "", "recipient number or address")
	text    = flag.String("text", "", "text message")
)

/*
	tasio -start -interval=1000

	tasio -send=GLOBE0 -address=+639353708662 -text="hello tasio"

	tasio -read

	tasio -devices
*/

func main() {
	flag.Parse()

	if *verbose {
		tasio.Verbose = true
	}

	if *start {
		tasio.Start(*interval)
	} else if *send != "" {
		tasio.Send(*send, *address, *text)
	} else if *read {
		mode := "all"
		if *new {
			mode = "new"
		}

		tasio.Read(mode)
	} else if *devices {
		tasio.Devices()
	} else if *install {
		tasio.Install()
	} else if *config {
		tasio.GetConfig(false)
	} else if *version {
		tasio.Version()
	} else if *stat {
		tasio.Stat()
	}
}
