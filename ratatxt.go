package tasio

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

// Rmsg ratatxt message
type Rmsg struct {
	Id      int    `json:"id"`
	Origin  string `json:"origin"`
	Address string `json:"address"`
	Text    string `json:"text"`
	Status  string `json:"status"`
}

func rrequest(method, endpoint, data string) ([]Rmsg, error) {
	var msgs []Rmsg

	endpoint = Config.Ratatxt.Host + endpoint
	req, err := http.NewRequest(method, endpoint, strings.NewReader(data))
	req.Header.Set("Application-Authorization", Config.Ratatxt.Token)

	log("request", method, endpoint, string(data))
	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		return msgs, err
	}

	defer r.Body.Close()

	log("response status is", r.Status)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return msgs, err
	}

	json.Unmarshal(body, &msgs)
	if err != nil {
		return msgs, err
	}

	log(msgs)

	return msgs, nil
}

// RPostInbox input new messages
func RPostInbox(origin, address, text string) {
	msg := Rmsg{
		0, origin, address, text, "pending",
	}

	data, err := json.Marshal(msg)
	if err != nil {
		return
	}

	_, err = rrequest("POST", "/user/inbox?filters[origin]="+origin, string(data))

	log(err)
}

// RGetOutbox send message
func RGetOutbox(origin string) []Rmsg {
	r, err := rrequest("GET", "/user/outbox?filters[origin]="+origin, "")
	if err != nil {
		log(err)
		return r
	}

	return r
}

// RUpdateOutbox update send message
func RUpdateOutbox(id int, status string) {
	data := `{"status": "` + status + `"}`
	_, err := rrequest("PUT", "/user/outbox/"+strconv.Itoa(id), data)

	log(err)
}
