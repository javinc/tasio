package tasio

import (
    "time"
    "strings"
    "encoding/hex"
    "github.com/xlab/at/sms"
    "github.com/xlab/at/util"
)

func Decode(s string) Message {
	log("PDU Decode")

	return pduDecode(s)
}

func Encode(address, text string) (int, string) {
	log("PDU Encode")

    return pduEncode(address, text)
}

// decode using xlab sms pdu decoder 
func pduDecode(s string) Message {
    s = strings.TrimSpace(s)
    // check add 0 if odd
    if len(s) % 2 == 1 {
        s += "0"
    }

    b, err := hex.DecodeString(s)
    if err != nil {
        panic(err)
    }

    p := new(sms.Message)
    p.ReadFrom(b)

    var m Message
	m.TimeStamp = time.Time(p.ServiceCenterTime)
	m.SCA = string(p.ServiceCenterAddress)
	m.Address = string(p.Address)
	m.Text = string(p.Text)

    return m
}

// encode using xlab sms pdu encoder
// using Message Type Submit
func pduEncode(address, text string) (int, string) {
    a := sms.PhoneNumber(address)
    m := sms.Message{
        Type:                 sms.MessageTypes.Submit,
        Text:                 text,
        Address:              a,
    }

    n, octets, err := m.PDU()
    if err != nil {
        panic(err)
    }

    // convert octets to string
    return n, util.HexString(octets)
}