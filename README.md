![cRq30pm1.png](https://bitbucket.org/repo/Az4x8e/images/3858663223-cRq30pm1.png)
# Tasio Project #

SMS API using GSM modems guided by queueing system as load balancer

### API ###

* GSM modem intreface written in Golang
* v0.7.0
* Ratatxt api integration

### Features ###

* send sms
* read sms
* received sms
* find devices
* multiple modems
* detect call event
* detect signal quality
* callback url option
* auto-config
* concurrent read and send per device

### Setup ###

* clone this repo using git
* `cd tasio/main`
* `go build main.go`
* rename the executable to `tasio`
* setup `config.json`

### Sample config.json ###
```
#!json

{
    "devices": [
        {
            "id": "GLOBE0",
            "url": "http://localhost:8080",
            "port": "/dev/ttyUSB0",
            "notif": "/dev/ttyUSB2",
            "number": "+639155649027",
            "imei": "123123123123"
        }
    ]
}
```

### Usage ###

* `tasio -start -interval=1000`

* `tasio -send=GLOBE0 -address="+639353708662" -text="hello tasio"`

* `tasio -read`

* `tasio -read -new`

* `tasio -stat`

* `tasio -config`

* `tasio -version`

* `tasio -verbose`

* `tasio -install`

### Platform supported & tested ###

* linux

### Change Log ###

* Ratatxt api integration
* usb reseter script added
* synchronous thread for each devices
* separate reading from sending
* sub-threads run synchronously
* verbose logs

### Road Map ###

* add time stamp on log
* log filing
