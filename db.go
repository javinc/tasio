package tasio

import (
    "github.com/jinzhu/gorm"
    _ "github.com/mattn/go-sqlite3"
)

type Db struct {
    Gorm gorm.DB
}

const (
	DB_FILE = "data.db"
)

func (x *Db) Init() error {
	log("Db Init")
	db, err := gorm.Open("sqlite3", getExecutableDir() + DB_FILE)

	if err != nil {
        exit("Db Error: ", err)
        
        return err
    }

    // settings
    // db.LogMode(Verbose)
	db.SingularTable(true)
	db.AutoMigrate(&Inbox{}, &Outbox{})

	// check if good
	err = db.DB().Ping()
	if err != nil {
        exit("Db Error: ", err)

        return err
	}

	// assign
	x.Gorm = db

	return nil
}

func (x *Db) CreateInbox(m Message, deviceId string) error {
	log("Db Create Inbox")
	r := Inbox{
		Device: deviceId,
		SCA: m.SCA,
		Address: m.Address,
		Text: m.Text,
		TimeStamp: m.TimeStamp,
	}

	if err := x.Gorm.Create(&r).Error; err != nil {
		log("Db Error: ", err)

		return err
	}

	return nil
}

func (x *Db) GetInbox() ([]Inbox, error) {
	log("Db Get Inbox")
	r := []Inbox{}
	if err := x.Gorm.Find(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	return r, nil
}

func (x *Db) GetInboxByStatus(s int) ([]Inbox, error) {
	log("Db Get Inbox by status")
	r := []Inbox{}
	if err := x.Gorm.Where("status = ?", s).Find(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	return r, nil
}

func (x *Db) UpdateInboxByStatus(id, s int) (Inbox, error) {
	log("Db Update Inbox")

	r := Inbox{
		ID: id,
	}

	if err := x.Gorm.First(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	// change status valiue
	r.Status = s

	if err := x.Gorm.Save(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	return r, nil
}

func (x *Db) CreateOutbox(m Message, deviceId string) error {
	log("Db Create Outbox")
	r := Outbox{
		Device: deviceId,
		SCA: m.SCA,
		Address: m.Address,
		Text: m.Text,
		TimeStamp: m.TimeStamp,
	}

	if err := x.Gorm.Create(&r).Error; err != nil {
		output("Db Error: ", err)

		return err
	}

	return nil
}

func (x *Db) GetOutbox() ([]Outbox, error) {
	log("Db Get Outbox")
	r := []Outbox{}
	if err := x.Gorm.Find(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	return r, nil
}

func (x *Db) GetOutboxByStatus(s int) ([]Outbox, error) {
	log("Db Get Outbox by status")
	r := []Outbox{}
	if err := x.Gorm.Where("status = ?", s).Find(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	return r, nil
}

func (x *Db) UpdateOutboxByStatus(id, s int) (Outbox, error) {
	log("Db Update Outbox")

	r := Outbox{
		ID: id,
	}

	if err := x.Gorm.First(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	// change status valiue
	r.Status = s

	if err := x.Gorm.Save(&r).Error; err != nil {
		output("Db Error: ", err)

		return r, err
	}

	return r, nil
}

func (x *Db) GetPending() (Outbox, error) {
	log("Db Get Pending")
	
	r := Outbox{}

	if err := x.Gorm.Where("status = ?", 0).First(&r).Error; err != nil {
		// output("Db Error: ", err)

		return r, err
	}

	return r, nil
}