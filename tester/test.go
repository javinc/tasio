package main

import (
	"log"
	"os/exec"
	"strconv"
	"strings"

	"bitbucket.org/javinc/tasio"

	"time"

	"github.com/haxpax/goserial"
)

const (
// PDU = "0791361907002039040C9136393507682600005150910294512306D3701BCE2E03"
)

func main() {
	// log.Println(getIndex("+CMGL: 2,1,,56"))

	// testNotif()
	// testSetup()

	testRatatxt()
}

func testRatatxt() {
	log.Println("testing ratatxt")

	origin := "639353708662"
	tasio.Verbose = true
	tasio.GetConfig(true)

	r := tasio.RGetOutbox(origin)
	for _, msg := range r {
		// update
		log.Println("updating", msg.Id)

		tasio.RUpdateOutbox(msg.Id, "done")
	}

	tasio.RPostInbox(origin, "09353708663", "from tasio")
}

func testSetup() {
	tasio.Install()
}

func testNotif() {
	tasio.Verbose = true
	modem := tasio.Modem{}
	modem.Name = "/dev/ttyUSB2"
	err := modem.Connect()
	if err != nil || modem.Conn == nil {
		return
	}

	for {
		log.Println(modem.Run("", true))
	}
}

func rawReadManual() {
	c := &serial.Config{Name: "/dev/ttyUSB6", Baud: 115200, ReadTimeout: time.Second * 5}
	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatal(err)
	}

	buf := make([]byte, 128)

	for {
		n, _ := s.Read(buf)
		log.Printf("%q", buf[:n])
	}
}

func getIndex(s string) int {
	i := -1

	// check prefix
	prefix := "+CMGL: "
	delimiter := ","
	if strings.Index(s, prefix) == 0 {
		// get first element
		rawDetails := strings.Split(s, delimiter)[0]
		index := strings.Trim(rawDetails, prefix)

		// cast index to int
		i, err := strconv.ParseInt(index, 0, 0)
		if err != nil {
			i = -1
		}

		return int(i)
	}

	return i
}

func rawRead() {
	modem := tasio.Modem{}
	modem.Name = "/dev/ttyUSB3"
	err := modem.Connect()
	if err != nil || modem.Conn == nil {
		return
	}

	log.Println(modem.Run("AT+CPMS?", true))
	log.Println(modem.Read())
	// log.Println(modem.Delete(0))
}

func testFindDevices() {
	// find device path using with prefix ttyUSB
	out, err := exec.Command("ls", "/dev/").Output()
	if len(out) == 0 || err != nil {
		log.Println("error", err)

		return
	}

	modems := make(map[string]tasio.Modem)

	dev := strings.Split(string(out), "\n")
	for _, row := range dev {
		// decode messages
		// if prefixed by 07 means its PDU
		if strings.Index(row, "ttyUSB") == 0 {
			// check if
			modem := tasio.Modem{}
			modem.Name = "/dev/" + row
			err := modem.Connect()
			if err != nil || modem.Conn == nil {
				continue
			}

			log.Println(row)
			// modem.Extras()
			// log.Println(modem.Read())
			// modem.Send("+639353708662", "sample")
			log.Println(modem.Run("AT+CPMS?", true))

			modems[row] = modem
		}
	}
}

func test1() {
	// tasio.Verbose = true

	// initialize db
	db := &tasio.Db{}
	err := db.Init()
	if err != nil {
		log.Fatal(err)
	}

	// initialize modem
	modem := &tasio.Modem{Name: "/dev/ttyUSB0"}
	err = modem.Connect()
	if err != nil {
		log.Fatal(err)
	}

	// send
	send, err := modem.Send("+639353708662", "hello")
	if err != nil {
		log.Println(err)
	}

	log.Println(send)

	// read
	read, err := modem.Read()
	if err != nil {
		log.Fatal(err)
	}

	log.Println(read)

	// delete first
	err = modem.Delete(0)
	if err != nil {
		log.Fatal(err)
	}
}
