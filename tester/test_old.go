package main

import (
	"encoding/hex"
	"fmt"
	"os/exec"
	"strings"
	"time"

	"github.com/javinc/tasio"
	"github.com/xlab/at/pdu"
	"github.com/xlab/at/sms"
	"github.com/xlab/at/util"
)

const (
	PDU = "0791361907002039040C9136393507682600005150910294512306D3701BCE2E03"
)

func main1() {
	// tasio.Verbose = true
	test5()
}

func test9() {
	tasio.Verbose = true
	tasio.InitDb()

	tasio.CreateInbox(tasio.Message{
		Address: "+639353708662",
		Text:    "Sample",
	})
}

func test8() {
	// tasio.Verbose = true

	// init modem
	modem := &tasio.Modem{}

	// set up modem
	modem.Name = "/dev/ttyUSB0"
	err := modem.Connect()
	if err != nil {
		fmt.Println("Modem Error:", err)

		return
	}

	// init db
	tasio.InitDb()

	loop := 1
	for loop > 0 {
		fmt.Println("reading in loop", loop)
		loop++

		// get msg queue with status 0
		q := tasio.GetPending()

		if q.ID != 0 {
			// send
			modem.Send(string(q.Address), string(q.Text))

			// set status to done
			tasio.UpdateInboxByStatus(int(q.ID), 1)
		}

		// get msgs
		msgs := modem.Read()
		for _, msg := range msgs {
			// save to db
			tasio.CreateInbox(msg)
		}

		// delete all
		if len(msgs) > 0 {
			modem.Delete()
		}

		// time.Sleep(1000 * time.Millisecond)
	}
}

func test7() {
	// tasio.Verbose = true

	// init modem
	modem := &tasio.Modem{}

	// set up modem
	modem.Name = "/dev/ttyUSB0"
	err := modem.Connect()
	if err != nil {
		fmt.Println("Modem Error:", err)

		return
	}

	// init db
	tasio.InitDb()

	loop := 1
	for loop > 0 {
		fmt.Println("reading in loop", loop)

		// get msgs
		msgs := modem.Read()
		for _, msg := range msgs {
			// save to db
			tasio.CreateInbox(msg)
		}

		// delete all
		if len(msgs) > 0 {
			modem.Delete()
		}

		loop++
	}
}

func test6() {
	// init modem
	modem := &tasio.Modem{}

	// set up modem
	modem.Name = "/dev/ttyUSB0"
	err := modem.Connect()
	if err != nil {
		return
	}

	// get message
	loop := 1
	for loop > 0 {
		fmt.Println("reading in loop", loop)

		// read all
		msgs := modem.Read()
		if len(msgs) > 0 {
			fmt.Println(msgs)

			// delete all
			modem.Delete()

			// send
			for _, row := range msgs {
				modem.Send(row.Address, row.Text)
			}

		}

		time.Sleep(0 * time.Millisecond)

		loop++
	}
}

func test5() {
	// find device path using with prefix ttyUSB
	out, err := exec.Command("ls", "/dev/").Output()
	if len(out) == 0 || err != nil {
		fmt.Println("error", err)

		return
	}

	modems := make(map[string]tasio.Modem)

	dev := strings.Split(string(out), "\n")
	for _, row := range dev {
		// decode messages
		// if prefixed by 07 means its PDU
		if strings.Index(row, "ttyUSB") == 0 {
			// check if
			modem := tasio.Modem{}
			modem.Name = "/dev/" + row
			err := modem.Connect()
			if err != nil || modem.Conn == nil {
				continue
			}

			fmt.Println(row)
			// modem.Extras()
			// modem.Read()

			modems[row] = modem
		}
	}
}

func test4() {
	s := sms.Message{
		Text:    "sample",
		Type:    sms.MessageTypes.Submit,
		Address: "+79269965690",
	}

	n, octets, err := s.PDU()
	if err != nil {
		panic(err)
	}

	fmt.Println("good")
	fmt.Println(n)
	fmt.Println(octets)
	fmt.Println(util.HexString(octets))
}

func test3() {
	s := "sample"

	userData := pdu.Encode7Bit(s)

	fmt.Println(s)
	fmt.Println(userData)
	fmt.Println(byte(len(userData)))
}

func test2() {
	// helper.JsonEncode(tasio.Decode(PDU))
}

func test1() {
	s := strings.TrimSpace(PDU)
	bs, err := hex.DecodeString(s)
	if err != nil {
		panic(err)
	}

	msg := new(sms.Message)
	msg.ReadFrom(bs)
	fmt.Println(msg)
}

func test0() {
	// verbose mode
	// tasio.Verbose = true

	// init modem
	modem := &tasio.Modem{}

	// set up modem
	modem.Name = "/dev/ttyUSB0"
	err := modem.Connect()
	if err != nil {
		return
	}

	// validate modem
	// modem.Verify()
	// modem.Extras()

	// get message

	modem.Read()

	// delete all messages
	modem.Delete()

	// send message
	modem.Send("+639353708662", "hello")
	modem.Send("+639155649027", "hello")
}
