package tasio

import (
    "fmt"
    "testing"
    "github.com/stretchr/testify/assert"
)

var (
    testDevice = "/dev/ttyUSB0"
)

func TestSend(t *testing.T) {
    addr := "+639353708662"
    text := "Hello Tasio"

    m := Modem{}
    m.Name = testDevice
    err := m.Connect()

    assert.NoError(t, err)
    assert.NotNil(t, m.Conn)
    
    resp, err := m.Send(addr, text)
    assert.NoError(t, err)

    fmt.Println("send", addr, text, resp)
}

func TestRead(t *testing.T) {
    m := Modem{}
    m.Name = testDevice
    err := m.Connect()

    assert.NoError(t, err)
    assert.NotNil(t, m.Conn)
    
    msgs, err := m.Read()
    assert.NoError(t, err)

    fmt.Println("read", msgs)
}

func TestDelete(t *testing.T) {
    m := Modem{}
    m.Name = testDevice
    err := m.Connect()

    assert.NoError(t, err)
    assert.NotNil(t, m.Conn)
    
    err = m.Delete(0)
    assert.NoError(t, err)

    fmt.Println("delete ok")
}