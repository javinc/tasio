package tasio

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
    d := Db{}
    err := d.Init()

    assert.NoError(t, err)
    assert.NotNil(t, d.Gorm)
}